package com.hotel.hotelservice.controller;

import com.hotel.hotelservice.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HotelController {

    private HotelService hotelService;

    @Autowired
    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @GetMapping(path = "/welcome")
    public ResponseEntity<String> getHotel(){

        return  ResponseEntity.ok(hotelService.getWelcomeMessage());
    }

    @GetMapping(path = "/hotels/{id}")
    public ResponseEntity<String> getHotelId(@PathVariable("id") String id){

        return ResponseEntity.ok(hotelService.getHotelById(id));
    }

    @GetMapping(path = "/hotels")
    public ResponseEntity<List<String>> getHotels(){

        return ResponseEntity.ok(hotelService.getHotels());
    }
}
